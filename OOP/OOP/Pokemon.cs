﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Pokemon
    {

        private int maxHP;


        public int MaxHP
        {
            get { return maxHP; }
            set
            {
                if (value < 20)
                {
                    maxHP = 20;
                }
                else if (value > 1000)
                {
                    maxHP = 1000;
                }
                else
                    maxHP = value;
            }
        }

        private int hP;

        public int HP
        {
            get { return hP; }
            set
            {
                if (value < 0)
                {
                    hP = 0;
                }
                else if (value > maxHP)
                {
                    hP = maxHP;
                }
                else
                    hP = value;
            }
        }

        public PokeSpecies PokeSpecies { get; set; }

        public PokeTypes PokeType { get; set; }

        public void Attack()
        {

            /* if (PokeType == PokeTypes.Grass)
             {
                 Console.ForegroundColor = ConsoleColor.Green;
             }
             Console.WriteLine($"{PokeSpecies.ToString().ToUpper()}!"); */

            switch (PokeType)  // swithc tab tab
            {
                case PokeTypes.Grass:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;

                case PokeTypes.Fire:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case PokeTypes.Water:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case PokeTypes.Electric:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                default:
                    break;
            }


            Console.WriteLine($"{PokeSpecies.ToString().ToUpper()}!");
        }

        public static void MakePokemon()
        {

            Pokemon Bulbasaur = new Pokemon();
            Bulbasaur.MaxHP = 20;
            Bulbasaur.HP = 20;
            Bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            Bulbasaur.PokeType = PokeTypes.Grass;

            Pokemon Charmander = new Pokemon();
            Charmander.MaxHP = 20;
            Charmander.HP = 20;
            Charmander.PokeSpecies = PokeSpecies.Charmander;
            Charmander.PokeType = PokeTypes.Fire;

            Pokemon Squirtle = new Pokemon();
            Squirtle.MaxHP = 20;
            Squirtle.HP = 20;
            Squirtle.PokeSpecies = PokeSpecies.Squirtle;
            Squirtle.PokeType = PokeTypes.Water;

            Pokemon Pikachu = new Pokemon();
            Pikachu.MaxHP = 20;
            Pikachu.HP = 20;
            Pikachu.PokeSpecies = PokeSpecies.Pikachu;
            Pikachu.PokeType = PokeTypes.Electric;

            Bulbasaur.Attack();
            Charmander.Attack();
            Squirtle.Attack();
            Pikachu.Attack();

        }

        public static Pokemon FirstConsiousPokemon(Pokemon[] pokemons)
        {
            foreach (Pokemon somePokemon in pokemons)
            {
                if (somePokemon.HP > 0)
                {
                    return somePokemon;
                }

            }
            return null;
        }

        public static void TestConsiousPokemon()
        {
            Pokemon Bulbasaur = new Pokemon();
            Bulbasaur.MaxHP = 20;
            Bulbasaur.HP = 0;
            Bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            Bulbasaur.PokeType = PokeTypes.Grass;

            Pokemon Charmander = new Pokemon();
            Charmander.MaxHP = 20;
            Charmander.HP = 0;
            Charmander.PokeSpecies = PokeSpecies.Charmander;
            Charmander.PokeType = PokeTypes.Fire;

            Pokemon Squirtle = new Pokemon();
            Squirtle.MaxHP = 20;
            Squirtle.HP = 2;
            Squirtle.PokeSpecies = PokeSpecies.Squirtle;
            Squirtle.PokeType = PokeTypes.Water;

            Pokemon Pikachu = new Pokemon();
            Pikachu.MaxHP = 20;
            Pikachu.HP = 20;
            Pikachu.PokeSpecies = PokeSpecies.Pikachu;
            Pikachu.PokeType = PokeTypes.Electric;



            Pokemon[] pokemons = { Bulbasaur, Charmander, Squirtle, Pikachu };
            //int[] pokehp = {Bulbasaur.HP, Charma}


            FirstConsiousPokemon(pokemons).Attack();
        }

        public static void TestConsiousPokemonSafe()
        {
            Pokemon Bulbasaur = new Pokemon();
            Bulbasaur.MaxHP = 20;
            Bulbasaur.HP = 0;
            Bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            Bulbasaur.PokeType = PokeTypes.Grass;

            Pokemon Charmander = new Pokemon();
            Charmander.MaxHP = 20;
            Charmander.HP = 0;
            Charmander.PokeSpecies = PokeSpecies.Charmander;
            Charmander.PokeType = PokeTypes.Fire;

            Pokemon Squirtle = new Pokemon();
            Squirtle.MaxHP = 20;
            Squirtle.HP = 0;
            Squirtle.PokeSpecies = PokeSpecies.Squirtle;
            Squirtle.PokeType = PokeTypes.Water;

            Pokemon Pikachu = new Pokemon();
            Pikachu.MaxHP = 20;
            Pikachu.HP = 0;
            Pikachu.PokeSpecies = PokeSpecies.Pikachu;
            Pikachu.PokeType = PokeTypes.Electric;



            Pokemon[] pokemons = { Bulbasaur, Charmander, Squirtle, Pikachu };


            if (FirstConsiousPokemon(pokemons) == null)
            {
                Console.WriteLine("alles KKKKo");

            }
            else
            {
                FirstConsiousPokemon(pokemons).Attack();
            }

            /*
            int count = 0;
            foreach (Pokemon item in pokemons)
            {
                if (item.HP != 0)
                {
                    count++;               
                }
            }

            if (count == 0)
            {
                Console.WriteLine("Alles KO");
            }
            else 
            {
                FirstConsiousPokemon(pokemons).Attack();
            } */
        }


        //FOUT
        public static void RestoreHPfout(int oldHP, int newHP)
        {
            oldHP = newHP;


        }

        //JUIST
        public static void RestoreHP(Pokemon pokemon, int newHP)
        {
            pokemon.HP = newHP;

            //oldHP = newHP is op de stack; datatype binne methode is kenbaar, maar buiten methode niet. is by value en niet met de referentie

        }


        public static void DemoRestoreHP()
        {
            Pokemon Bulbasaur = new Pokemon();
            Bulbasaur.MaxHP = 20;
            Bulbasaur.HP = 0;
            Bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
            Bulbasaur.PokeType = PokeTypes.Grass;

            Pokemon Charmander = new Pokemon();
            Charmander.MaxHP = 20;
            Charmander.HP = 0;
            Charmander.PokeSpecies = PokeSpecies.Charmander;
            Charmander.PokeType = PokeTypes.Fire;

            Pokemon Squirtle = new Pokemon();
            Squirtle.MaxHP = 20;
            Squirtle.HP = 0;
            Squirtle.PokeSpecies = PokeSpecies.Squirtle;
            Squirtle.PokeType = PokeTypes.Water;

            Pokemon Pikachu = new Pokemon();
            Pikachu.MaxHP = 20;
            Pikachu.HP = 0;
            Pikachu.PokeSpecies = PokeSpecies.Pikachu;
            Pikachu.PokeType = PokeTypes.Electric;



            Pokemon[] pokemons = { Bulbasaur, Charmander, Squirtle, Pikachu };

            for (int i = 0; i < pokemons.Length; i++)
            {
                Pokemon.RestoreHP(pokemons[i], pokemons[i].MaxHP);
            }

            for (int i = 0; i < pokemons.Length; i++)
            {
                Console.WriteLine(pokemons[i].HP);
            }
        }
    }
}

    

