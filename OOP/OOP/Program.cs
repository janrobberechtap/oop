﻿using System;
using OOP.Distance;
using OOP.Geometry;


namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {

                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. Vormen tekenen (h8-vormen)");
                Console.WriteLine("2. Auto's laten rijden (h8-autos)");
                Console.WriteLine("3. Patiënten tonen (h8-patienten) ");
                Console.WriteLine("4. Honden laten blaffen (h8-blaffende-honden)");
                Console.WriteLine("5. Days of the week");
                Console.WriteLine("6. Ticks van 2000");
                Console.WriteLine("7. LeapYear");
                Console.WriteLine("8. ArrayTimerProgram");
                Console.WriteLine("9. ResultV1");
                Console.WriteLine("10. ResultV2");
                Console.WriteLine("11. Pokemon");
                Console.WriteLine("12. Consious Pokemon");
                Console.WriteLine("13. Consious Pokemon Safe");
                Console.WriteLine("14. Pokemon - DemoRestoreHP");



                int choice = int.Parse(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        ShapesBuilder builder = new ShapesBuilder();
                        builder.Color = ConsoleColor.Green;
                        //builder.Symbol = '*';
                        Console.Write(builder.Line(20));
                        Console.WriteLine();
                        Console.WriteLine();
                        builder.Color = ConsoleColor.Red;
                        builder.Symbol = '-';
                        Console.Write(builder.Rectangle(5, 10));
                        Console.WriteLine();
                        Console.WriteLine();
                        builder.Color = ConsoleColor.Blue;
                        builder.Symbol = '@';
                        Console.Write(builder.Triangle(10));
                        Console.ReadKey();
                        Console.ResetColor();
                        Console.Clear();
                        break;

                    case 2:
                        Car car1 = new Car();
                        double startSpeedCar1 = 0.00D;
                        int timesGasCar1 = 5;
                        int timesBrakeCar1 = 3;
                        car1.Speed = startSpeedCar1;
                        car1.Speed = car1.Gas(timesGasCar1);
                        car1.Speed = car1.Brake(timesBrakeCar1);
                        double endSpeedCar1 = car1.Speed;
                        double distanceCar1 = car1.Odometer(timesGasCar1, timesBrakeCar1, startSpeedCar1, endSpeedCar1);


                        Car car2 = new Car();
                        double startSpeedCar2 = 0.00D;
                        int timesGasCar2 = 9;
                        int timesBrakeCar2 = 1;
                        car2.Speed = startSpeedCar2;
                        car2.Speed = car2.Gas(timesGasCar2);
                        car2.Speed = car2.Brake(timesBrakeCar2);
                        double endSpeedCar2 = car2.Speed;
                        double distanceCar2 = car2.Odometer(timesGasCar2, timesBrakeCar2, startSpeedCar2, endSpeedCar2);

                        Console.WriteLine();
                        Console.WriteLine("auto 1: {0}km/u, {1}km", endSpeedCar1, Math.Round(distanceCar1, 2));
                        Console.WriteLine("auto 2: {0}km/u, {1}km", endSpeedCar2, Math.Round(distanceCar2, 2));

                        Console.ReadKey();
                        Console.ResetColor();
                        Console.Clear();

                        break;

                    case 3:

                        // klasse van objecten
                        Patient[] patienten = new Patient[5];

                        patienten[0].Name = "test";
                        patienten[0].Gender = true; //true is man
                        patienten[1].Name = "jack";
                        patienten[1].Gender = false;



                        Console.WriteLine("Hoe veel patiënten zijn er?");
                        int numberOfPatients = int.Parse(Console.ReadLine());
                        string[] patientNames = new string[numberOfPatients];
                        string[] patientGenders = new string[numberOfPatients];
                        string[] patientLifestyles = new string[numberOfPatients];
                        int[] patientDays = new int[numberOfPatients];
                        int[] patientMonths = new int[numberOfPatients];
                        int[] patientYears = new int[numberOfPatients];

                        for (int i = 0; i < numberOfPatients; i++)
                        {
                            Patient person = new Patient();


                            patientNames[i] = Console.ReadLine();
                            patientGenders[i] = Console.ReadLine();
                            patientLifestyles[i] = Console.ReadLine();
                            patientDays[i] = int.Parse(Console.ReadLine());
                            patientMonths[i] = int.Parse(Console.ReadLine());
                            patientYears[i] = int.Parse(Console.ReadLine());
                        }
                        // afprinten van het verslag
                        // variabele in twee keer toegekend om code niet te breed te maken
                        for (int i = 0; i < numberOfPatients; i++)
                        {
                            string info = $"{patientNames[i]} ({patientGenders[i]}, {patientLifestyles[i]})";
                            info += $", geboren {patientDays[i]}-{patientMonths[i]}{patientYears[i]}";
                            Console.WriteLine(info);
                        }


                            Console.ReadKey();
                        Console.ResetColor();
                        Console.Clear();

                        break;

                    case 4:
                        Console.WriteLine("dit is 4");
                        Console.ReadKey();
                        Console.ResetColor();
                        Console.Clear();
                        break;

                    case 5:

                        DayOfWeekProgram.Main();
                                               
                        break;

                    case 6:

                        Ticks2000Program.Main();
                        break;

                    case 7:

                        LeapYearProgram.Main();
                        break;

                    case 8:

                        ArrayTimerProgram.Main();
                        break;

                    case 9:

                        ResultV1.Main();
                        break;

                    case 10:

                        ResultV2.Main();
                        break;

                    case 11:

                        Pokemon.MakePokemon();
                        break;

                    case 12:
                        Pokemon.TestConsiousPokemon();
                        
                        break;

                    case 13:
                        Pokemon.TestConsiousPokemonSafe();
                        break;

                    case 14:
                        Pokemon.DemoRestoreHP();
                        break;

                    default:
                        Console.WriteLine("ongeldig getal");
                        Console.ReadKey();
                        Console.ResetColor();
                        Console.Clear();
                        break;

                }

                Console.ReadKey();
            }
        }
    }
}
