﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ArrayTimerProgram
    {
        public static void Main()
        {

            DateTime start = DateTime.Now;

            int[] myArr = new int[1000000];

            for (int i = 0; i == myArr.Length; i++)
            {
                myArr[i] = i + 1;
            }

            DateTime end = DateTime.Now;

            TimeSpan ts = end - start;

            Console.WriteLine($"Dit duur {ts.Milliseconds} milliseconden lang");



        }

    }
}
