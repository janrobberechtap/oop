﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Distance

{
    class Car
    {
        private double speed;

        public double Speed
        {
            get 
            {
                         
                return speed; 
            }
            set
            {

                if (value >= 0 && value <= 120)
                {
                    speed = value;
                }
                else
                {
                    speed = 120;
                }
            }

        }

        public double Gas(int times)
        {
            return Gas(times, speed);
        }

        public double Gas(int times, double speed)
        {
            double newSpeedGas = speed + (times * 10);
            return newSpeedGas;
        }


        public double Brake(int times)
        {
            return Brake(times, speed);
        }

        public double Brake(int times, double speed)
        {
            double newSpeedBrake = speed - (times * 10);
            return newSpeedBrake;
        }


        public double Odometer(int timesGas, int timesBrake, double startSpeed, double endSpeed )
        {
            int minutesDriving = timesGas + timesBrake;
            double avgSpeed = (endSpeed - startSpeed) / 2;
            double distance = (avgSpeed / 60) * minutesDriving;
            return distance;
        }
    }
}
