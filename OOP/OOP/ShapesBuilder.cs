﻿using System;
using System.Collections.Generic;
using System.Text;



namespace OOP.Geometry
{
    class ShapesBuilder
    {

        private ConsoleColor color;

        public ConsoleColor Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                Console.ForegroundColor = color;
            }
        }

        private char symbol = 'R';

        public char Symbol
        {
            get
            {
                return symbol;
            }
            set
            {
                symbol = value;
            }
        }

        public string Line(int length)
        {
            return new string(symbol, length);
        }

        public string Rectangle(int height, int width)
        {
            return Rectangle(height, width, symbol);
        }

        public string Rectangle(int height, int width, char symbol) //, char symbol) 
        {
            string output = "";
            int i;
            for (i = 0; i < height; i++)
            {
                output += Line(width);  //,symbol)
                if (i < height - 1)
                {
                    output += "\n";
                }

            }
            return output;
        }

        public string Triangle(int height)
        {
            return Triangle(height, symbol);
        }

        public string Triangle(int height, char symbol) //, char symbol) 
        {
            string output = "";
            int i;
            for (i = 0; i < height; i++)
            {
               output += Line(i);  //,symbol)
               if (i < height - 1)
               {
                  output += "\n";
               }

                }
                return output;

            }

    }
}
