﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Ticks2000Program
    {

        public static void Main()
        {

            DateTime now = DateTime.Now;
            DateTime firstDay = new DateTime(2000, 1, 1);
            CultureInfo duthcCI = new CultureInfo("nl-BE");

            TimeSpan difference = now - firstDay;
            Console.WriteLine($"Sinds 1 januari 2000 zijn er {difference.Ticks} voorbijgegaan");

        }

    }
}
