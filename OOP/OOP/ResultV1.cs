﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV1
    {
        

        public byte Score { get; set; }


    
        public void PrintHonors()
        {

            if (Score < 50)
            {
                Console.WriteLine("niet geslaagd");
            }
            else if (Score < 68)
            {
                Console.WriteLine("voldoende");
            }
            else if (Score < 75)
            {
                Console.WriteLine("onderscheiding");
            }
            else if (Score < 85)
            {
                Console.WriteLine("grote onderscheiding");
            }
            else
            {
                Console.WriteLine("grootste onderscheiding");
            }
         
        }

        public static void Main()
        {
            ResultV1 result1 = new ResultV1();
            result1.Score = 70;
            result1.PrintHonors();

            ResultV1 result2 = new ResultV1();
            result2.Score = 40;
            result2.PrintHonors();
        }




    }
}
