﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV2
    {
        public byte Score { get; set; }



        public Honors ComputeHonors()
        {

            if (Score < 50)
            {
                return Honors.NietGeslaagd;
            }
            else if (Score < 68)
            {
                return Honors.Voldoende;
            }
            else if (Score < 75)
            {
                return Honors.Onderscheiding;
            }
            else if (Score < 85)
            {
                return Honors.GroteOnderscheiding;
            }
            else
            {
                return Honors.GrootsteOnderscheiding;
            }

        }

        public static void Main()
        {
            ResultV2 result1 = new ResultV2();
            result1.Score = 70;
            Console.WriteLine(result1.ComputeHonors());

            ResultV2 result2 = new ResultV2();
            result1.Score = 40;
            Console.WriteLine(result1.ComputeHonors());
        }


    }
}
