﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Patient
    {


		private string name;

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		private bool gender;

		public bool Gender
		{
			get { return gender; }
			set { gender = value; }
		}

		private string lifestyle;

		public string Lifestyle
		{
			get { return lifestyle; }
			set { lifestyle = value; }
		}

		private int day;

		public int Day
		{
			get { return day; }
			set { day = value; }
		}

		private int month;

		public int Month
		{
			get { return month; }
			set { month = value; }
		}

		private int year;

		public int Year
		{
			get { return year; }
			set { year = value; }
		}



	}
}
