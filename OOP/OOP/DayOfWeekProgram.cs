﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class DayOfWeekProgram
    {


        public static void Main()
        {
            Console.WriteLine("Welke dag?");
            int day = int.Parse(Console.ReadLine());
            Console.WriteLine("Welke maand?");
            int month = int.Parse(Console.ReadLine());
            Console.WriteLine("Welk jaar?");
            int year = int.Parse(Console.ReadLine());

            DateTime chosenDay = new DateTime(year, month, day);
            CultureInfo duthcCI = new CultureInfo("nl-BE"); //using System.Globalization;

            Console.WriteLine($"{chosenDay.ToString("d MMMM yyyy")} is een {chosenDay.ToString("dddd")}");

            //Console.WriteLine(chosenDay.ToString("D"));
            // op belgisch zetten

            // Console.WriteLine($"{chosenDay.ToString("D")} is een {chosenDay.DayOfWeek}");
        }

    }
}
